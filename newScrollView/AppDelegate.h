//
//  AppDelegate.h
//  newScrollView
//
//  Created by Mohammad Jebelli on ۲۰۱۷/۱/۱۲.
//  Copyright © ۲۰۱۷ cs3260. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

