//
//  ViewController.m
//  newScrollView
//
//  Created by Mohammad Jebelli on ۲۰۱۷/۱/۱۲.
//  Copyright © ۲۰۱۷ cs3260. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    UIScrollView* scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollview.backgroundColor = [UIColor blueColor];
    [self.view addSubview:scrollview];
    
    [scrollview setContentSize:CGSizeMake( self.view.frame.size.width * 3, self.view.frame.size.height * 3)];
    scrollview.pagingEnabled = YES;
    
    
    
    UIView* redView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollview.frame.size.width, scrollview.frame.size.height)];
    redView.backgroundColor = [UIColor redColor];
    [scrollview addSubview:redView];
    
    
    
    
    UIView* yellowView = [[UIView alloc]initWithFrame:CGRectMake(413, 0, scrollview.frame.size.width, scrollview.frame.size.height)];
    yellowView.backgroundColor = [UIColor yellowColor];
    [scrollview addSubview:yellowView];
   
    
    
    
    UIView* blackView = [[UIView alloc]initWithFrame:CGRectMake(0, 735, scrollview.frame.size.width, scrollview.frame.size.height)];
    blackView.backgroundColor = [UIColor blackColor];
    [scrollview addSubview:blackView];
    
    
    
    UIView* greenView = [[UIView alloc]initWithFrame:CGRectMake(413, 735, scrollview.frame.size.width, scrollview.frame.size.height)];
    greenView.backgroundColor = [UIColor greenColor];
    [scrollview addSubview:greenView];
    
    
    UIView* purpleView = [[UIView alloc]initWithFrame:CGRectMake(826, 735, scrollview.frame.size.width, scrollview.frame.size.height)];
    purpleView.backgroundColor = [UIColor purpleColor];
    [scrollview addSubview:purpleView];
    
    
    
    UIView* whiteView = [[UIView alloc]initWithFrame:CGRectMake(0, 1470, scrollview.frame.size.width, scrollview.frame.size.height)];
    whiteView.backgroundColor = [UIColor whiteColor];
    [scrollview addSubview:whiteView];
    
    
    UIView* orangeView = [[UIView alloc]initWithFrame:CGRectMake(413, 1470, scrollview.frame.size.width, scrollview.frame.size.height)];
    orangeView.backgroundColor = [UIColor orangeColor];
    [scrollview addSubview:orangeView];
    
    
    
    UIView* grayView = [[UIView alloc]initWithFrame:CGRectMake(826, 1470, scrollview.frame.size.width, scrollview.frame.size.height)];
    grayView.backgroundColor = [UIColor grayColor];
    [scrollview addSubview:grayView];



}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
